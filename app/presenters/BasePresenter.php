<?php

namespace App\Presenters;

use Nette;
use App\Model;
use App\Model\plt;

/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends Nette\Application\UI\Presenter
{

  /**
    * @inject
    * @var \App\Model\plt
    */
  public $plt;

  protected function beforeRender(){
    $this->pigFilter();
  }

  public function pigFilter(){
    $this->template->addFilter('piglatin', function($n) {
      return $this->plt->translate($n);
    });
  }

}
