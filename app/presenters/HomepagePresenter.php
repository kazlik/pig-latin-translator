<?php

namespace App\Presenters;

use Nette,
    Nette\Application\UI;


class HomepagePresenter extends BasePresenter
{

  public function renderDefault(){
    //dump($this->plt->translate('hello beast dough happy question star three spray prays'));
  }

  protected function createComponentTranslatorForm()
  {
    $form = new UI\Form;
    $form->addText('text', 'String to translate:');
    $form->addSubmit('send', 'Translate!')
      ->setOmitted(true);
    $form->onSuccess[] = [$this, 'translatorFormSucceeded'];
    return $form;
  }

  public function translatorFormSucceeded(UI\Form $form, $values)
  {
    $this->flashMessage($this->plt->translate($values->text));
  }
}
