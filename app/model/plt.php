<?php
namespace App\Model;

class plt
{
  //letter 'y' is missing; in first check it isn't wovel and in second check it isn't consonant
  private $wovel     = '/[aeiouAEIOU]/';    //For 1st check if word starts on wovel
  private $consonant = '/[^aeiouyAEIOUY]/'; //For 2nd+ check if 2nd+ letter is consonant
  private $capital   = '/[A-Z]/';           //For capitalizing words

  public function translate($string){
    $return = null;

    //Split string into words [1] and other characters [2]
    preg_match_all('/([a-zA-Z]+)?([\s\W\d]+)?/', $string, $string);
    foreach($string[1] as $id => $word){
      if($word != ''){
        //get type of the word: small, firstBig, other
        $num = null;
        for($a = 0; $a < strlen($word); $a++){
          preg_match($this->capital, $word[$a]) ? $num[$a] = true : $num[$a] = false;
        }
        $sum = array_sum($num);
        if($sum == 0){
          $wordsize = 'small';
        }elseif($sum == 1 && $num[0] == 1){
          $wordsize = 'firstBig';
        }else{
          $wordsize = 'other';
        }

        //If first wovel
        if(preg_match($this->wovel, $word[0])){
          $newword = $word . '-way';
        //else
        }else{
          //If first letter is big, make new 1st letter big
          if($wordsize == 'firstBig'){
            for($a = 0; $a < strlen($word); $a++){
              if(!preg_match($this->consonant, $word[$a]))
              {
                $word[$a] = strtoupper($word[$a]);
                break;
              }
            }
            $word[0] = strtolower($word[0]);
          }
          //If word starts with 'qu'
          if(preg_match('/[qQ][uU]/', $word[0] . $word[1])){
            $newword = substr($word, 2);
            $newword .= '-' . $word[0] . $word[1] . 'ay';
          //rest of the words
          }else{
            //Split word before 1st wovel
            for($a = 1; $a < strlen($word); $a++){
              if(!preg_match($this->consonant, strtolower($word[$a]))){
                break;
              }
            }
              $newword = substr($word, $a);
              $newword .= '-' . substr($word, 0, $a) . 'ay';
          }
        }
        //If last Pig Latin letter is capital, make capital affix '...AY'
        switch($wordsize){
          case 'other':
            if(preg_match($this->capital, $newword[strlen($newword)-3])){
              $newword[strlen($newword)-2] = strtoupper($newword[strlen($newword)-2]);
              $newword[strlen($newword)-1] = strtoupper($newword[strlen($newword)-1]);
            }
          break;
        }
        //join word + other characters back
        $return .= $newword . $string[2][$id];
      //If not word, only copy input into return
      }else{
        $return .= $word . $string[2][$id];
      }
    }
    return $return;
  }

}