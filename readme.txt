Pig Latin Translator
====================
Info o Pig Latin nap�. na wiki: https://cs.wikipedia.org/wiki/Pig_Latin

Pou�it�
=======
Pig Latin Translator (d�le jako PLT) lze pou��t dv�mi zp�soby:
1. V presenteru pomoc� funkce $this->plt->translate($string);
2. V Latte �ablon� pomoc� filtru piglatin {$string|piglatin}

Co to um�?
==========
Program um� krom� b�n�ho p�ekladu slov z angli�tiny do pig latiny i p�r vychyt�vek:
1. Pokud slovo obsahuje jedno velk� p�smeno na prvn� pozici (nap�. na za��tku v�ty, jm�no, n�zev),
   tak velk� p�smeno p�eklada� p�esune na prvn� p�smeno v pig latin� (nap�. Hello nep�elo�� jako ello-Hay, ale jako Ello-hay)
2. Pokud je ve slov� v�ce velk�ch p�smen nebo jedno p�smeno na jin� ne� prvn� pozici, k ��dn� zm�n� nedoch�z�.
   Pouze jestli je posledn� p�smeno p�ed afixem 'ay' velk�, tak se afix nap�e velk�mi p�smeny (nap�. StRing: ing-StRAY)
3. P�eklada� zachov�v� interpunkci a ostatn� znaky mezi slovy, proto lze p�ekl�dat cel� v�ty bez ztr�ty t�chto znam�nek.